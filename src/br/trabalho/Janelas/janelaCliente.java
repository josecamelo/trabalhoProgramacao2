
package br.trabalho.Janelas;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.trabalho.repositorio.clienteBD;
import java.util.logging.Level;
import java.util.logging.Logger;



public class janelaCliente extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	
        private  JLabel codclienteLabel;
        private  JTextField codclienteTextField;
        
	private JLabel cpfLabel;
	private JTextField cpfTextField;
	
	private JLabel nomeLabel;
	private JTextField nomeTextField;
        
       
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private clienteBD pB = new clienteBD();
	
	public janelaCliente() {
		super(" cadastro cliente!!");
                
		
                nomeLabel = new JLabel("Nome");
		nomeTextField = new JTextField(30);
                
               
                
		cpfLabel = new JLabel("cpfcliente");
		cpfTextField = new JTextField(10);
                
                codclienteLabel =new JLabel("codigo");
                codclienteTextField =new JTextField(10);
		
		
		
		cadastroPanel = new JPanel(new GridLayout(4,1));
                                
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
                
                               
                cadastroPanel.add(codclienteLabel);
                cadastroPanel.add(codclienteTextField);
                
		cadastroPanel.add(cpfLabel);
		cadastroPanel.add(cpfTextField);
		
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener((ActionEvent arg0) -> {
                    // TODO Auto-generated method stub
                    try {
                        pB.inserir(nomeTextField.getText(), cpfTextField.getText(),codclienteTextField.getText());
                        //pB.inserir(nomeTextField.getText(),codclienteTextField.getText(),cpfTextField.getText());
                        JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
                    }catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    // TODO Auto-generated catch block
                });
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				nomeTextField.setText("");
				nomeTextField.setText(pB.listar(cpfTextField.getText()));			
			}
			
		});		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pB.apagar(nomeTextField.getText(),cpfTextField.getText(),codclienteTextField.getText());
                    } catch (SQLException ex) {
                        Logger.getLogger(janelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //pB.apagar(codclienteTextField.getText());
                    nomeTextField.setText("");
                    cpfTextField.setText("");
                    
                    JOptionPane.showMessageDialog(null,"Registro excluído com sucesso!!");
                });		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener((ActionEvent arg0) -> {
                    try {
                        // TODO Auto-generated method stub
                        pB.atualizar(nomeTextField.getText(), cpfTextField.getText(), codclienteTextField.getText());
                        //pB.atualizar(nomeTextField.getText(), cpfTextField.getText(), SOMEBITS);
                    } catch (SQLException ex) {
                        Logger.getLogger(janelaCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //pB.atualizar(nomeTextField.getText(),cpfTextField.getText(),codclienteTextField.getText());
                    JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
                });		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener((ActionEvent arg0) -> {
                    JanelaListagem jl=new JanelaListagem();
                });
		
		botoesPanel = new JPanel(new GridLayout(1,5));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		janelaCliente jc = new janelaCliente();
	}
	
}
