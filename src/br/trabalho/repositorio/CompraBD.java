
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author camelobboy
 */
public class CompraBD {
    
 public void inserir(String quantidadeCompra,String idCompra) throws Exception {
     try (Connection conn = ConexaoBancoDeDados.getConnection()) {
         if(conn !=null){
             System.out.println("conexao feita ");
         }
         try (PreparedStatement stmt = conn.prepareStatement("insert into compra values(?,?)")) {
             stmt.setDouble(2, Double.parseDouble(quantidadeCompra));
             stmt.setInt(1, Integer.parseInt(idCompra));
             
             
             
             if (quantidadeCompra.equals("")) {
                 throw new Exception("quantidade não pode ser vazio!!");
             } else {
                 stmt.executeUpdate();
             }
         }
     }
	}
	
	public void apagar(String idItem) throws SQLException {
     try (Connection conn = ConexaoBancoDeDados.getConnection()) {
         PreparedStatement stmt = conn.prepareStatement("delete from compra where idCompra=?");
         stmt.setInt(1, Integer.parseInt(idItem));
         stmt.executeUpdate();
         stmt.close();
     }		
	}
	
	public void atualizar(String quantidadeCompra,String idCompra)throws SQLException {
     try (Connection conn = ConexaoBancoDeDados.getConnection()) {
         PreparedStatement stmt = conn.prepareStatement("update compra set idItem=? where quantidadeCompra=?");
         stmt.setInt(1, Integer.parseInt(idCompra));;
         stmt.setDouble(2, Double.parseDouble(quantidadeCompra));
         stmt.executeUpdate();
         stmt.close();
     }
	}
	
	public List<String> listarTodasCompra(){
		List<String> itenscompra = new ArrayList<String>();
		try {
			Connection conn = ConexaoBancoDeDados.getConnection();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from compra");
			while (rs.next()) {
				itenscompra.add(rs.getString(2));
			}
		} catch (SQLException e) {
		}
		return itenscompra;
	}
	
	public String listar(String idCompra) {
		String itenscompra="";
		
		try {
			Connection conn = ConexaoBancoDeDados.getConnection();
			PreparedStatement stmt = conn.prepareStatement("select quantidadeCompra from compra where idCompra=?");	
			stmt.setInt(1, Integer.parseInt(idCompra));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				itenscompra = rs.getString(1);
			}
			if (itenscompra.equals("")){
				throw new Exception("O nome da compra está em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return itenscompra;
	}

    
    }
	


    

