/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author camelobboy
 */
public class ClienteDAO {
    public int cadastrar(String nome,String cpf,String codCliente) throws SQLException {
            
            
           
                  Connection con = ConexaoBancoDeDados.getConnection();
		
		PreparedStatement pStatement = con.prepareStatement("INSERT INTO cliente VALUES(?,?,?)");
		
		
		pStatement.setString(1, nome);
                pStatement.setString(1, cpf);
		pStatement.setInt(2, Integer.parseInt(codCliente));
		
		
		
		int resultado = pStatement.executeUpdate();
		
			pStatement.close();
		
		
		con.close();
		
		
		return resultado;
	}
	
}

