
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author camelobboy
 */
public class clienteBD {
    
    public void inserir(String nome,String cpf, String codcliente) throws Exception {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("insert into cliente values(?,?,?)")) {
            
            stmt.setString(1, nome);
            stmt.setString(2, cpf);
            stmt.setInt(3, Integer.parseInt(codcliente));
            
            if (nome.equals("")) {
                throw new Exception("Nome não pode ser vazio!!");
            } else {
                stmt.executeUpdate();
                 stmt.close();
	         conn.close();
            }
        }
	}
	
	public void apagar(String nome,String cpf,String codcliente) throws SQLException {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("delete from cliente where nome=?")) {
            
            stmt.setString(1, nome);
            stmt.setString(2, cpf);
            stmt.setInt(3,Integer.parseInt(codcliente));
            stmt.executeUpdate();
            stmt.close();
	    conn.close();
        }		
	}
	
	public void atualizar(String nome, String cpf,String codcliente) throws SQLException {
        try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("update cliente set nome=? where sobrenome=?")) {
            stmt.setString(1, nome);
            stmt.executeUpdate();
            stmt.close();
	    conn.close();
        }
	}
	
	public List<String> listarTodos(){
		List<String> nomecliente = new ArrayList<String>();
		try {
			Connection conn = ConexaoBancoDeDados.getConnection();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from cliente");
			while (rs.next()) {
				nomecliente.add(rs.getString(2));
			}
		} catch (SQLException e) {
		}
		return nomecliente;
	}
	
	public String listar(String codcliente) {
		String nomeCliente="";
		
		try {
                    try (Connection conn = ConexaoBancoDeDados.getConnection(); PreparedStatement stmt = conn.prepareStatement("select nome from cliente where codcliente=?")) {
                        stmt.setInt(1,Integer.parseInt(codcliente));
                        ResultSet rs = stmt.executeQuery();
                        if (rs.next()) {
                            nomeCliente = rs.getString(1);
                        }
                        if (nomeCliente.equals("")){
                            throw new Exception("O nome do cliente está em branco!!");
                        }
                    }
		} catch (SQLException e) {
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomeCliente;
	}


}