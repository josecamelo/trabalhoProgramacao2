
package br.trabalho.repositorio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author camelobboy
 */
public class ProdutoDAO {
    
 public int cadastrar(String quantidadeProduto,String nomeProduto,String idProduto) throws SQLException {
            
            
           
                  Connection con = ConexaoBancoDeDados.getConnection();
		
		PreparedStatement pStatement = con.prepareStatement("INSERT INTO produto VALUES(?,?.?)");
		
		
		pStatement.setString(1, quantidadeProduto);
		pStatement.setInt(2, Integer.parseInt(idProduto));
		
		
		
		int resultado = pStatement.executeUpdate();
		
			pStatement.close();
		
		
		con.close();
		
		
		return resultado;
	}
	
}

    


